# echo server

To connect to the server *socat* or *netcat* can be used at least on Linux or macOS.

- Default TCP listener port is *1234*

```bash
    socat -v tcp-l:1234,fork exec:'/bin/cat'
```

less or other tools can be used as alternatives to cat.

## Work to be done

- [x] successful connection
- [x] bounce the message
- [ ] create an asynchronous connection with std or Tokio 
- [ ] maybe some tests
