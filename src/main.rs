use std::borrow::Cow;
use std::io::{prelude::*, stdin};
use std::net::TcpStream;

const ECHO_SERVER: &str = "127.0.0.1:1234";

fn main() {
    println!("connecting to {}",ECHO_SERVER);
    // Create a tcp stream of data
    let stream = TcpStream::connect(ECHO_SERVER);
    // error handling to tcp connection over port 1234
    match stream {
        // successful connection
        // st should be mutable  
        Ok( mut st) => {
            println!("connected to {}:{}",
            st.local_addr().unwrap().ip(),
            st.local_addr().unwrap().port()
                );
            // message to be bounced by the server 
            loop {
                println!("message to be sent: ");
                let mut msg: String = String::new();
                match stdin().read_line( &mut msg){
                    Ok(msg) => msg,
                    Err(e) => panic!("Oops {}",e)
                };
                // message should be converted from str to u8
                // fn write(&mut self, buf: &[u8]) -> io::Result<usize>
                // write function takes &[u8] as input and returns a Result
                let _ = st.write(msg.as_bytes());
                // flush ensures the buffers reach their destination
                let _ = st.flush();
                println!("sent: {}",msg);

                //read the result 
                // create a buffer of 1Kb
                let mut buffer: [u8; 1024] = [0; 1024];
                // get the length of the sent message 
                let _: usize = st.read(&mut buffer).unwrap();
                // utf_lossy can handle invalid characters
                // String::from_utf8 can be used also
                let message: Cow<'_, str> = String::from_utf8_lossy(&buffer);
                println!("received : {}", message);
            }
        }
        // failed connection
        _ => print!("not connected ")
    };
}
